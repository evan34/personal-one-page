<?php

class PictureItem {
    public $url;
    public $alt;

    function __construct($url, $alt){
        $this->url = $url;
        $this->alt = $alt;
    }
}