<?php

class CardService {
    public $html = '';
    public $modals = '';

    function __construct(
        string $id,
        string $fontawesomeLogo,
        PictureItem $picture,
        string $cardTitle,
        string $cardDescription,
        string $projectName,
        string $modalSubTitle,
        string $modalBody,
        array $modalCarouselItemList,
        array $liList,
        string $closeModalText
    ){
        $this->html = '
            <div class="col-lg-4 col-sm-6 mb-4">
                <div class="portfolio-item">
                    <a class="portfolio-link" data-toggle="modal" href="#modalId'.$id.'">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content"><i class="fas '.$fontawesomeLogo.' fa-3x"></i></div>
                        </div>
                        <img class="img-fluid" src="assets/img/'.$picture->url.'" alt="'.$picture->alt.'" />
                    </a>
                    <div class="portfolio-caption">
                        <div class="portfolio-caption-heading">'.$cardTitle.'</div>
                        <div class="portfolio-caption-subheading text-muted">'.$cardDescription.'</div>
                    </div>
                </div>
            </div>
        ';

        $this->modals = '
            <div class="portfolio-modal modal fade" id="modalId'.$id.'" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="close-modal" data-dismiss="modal"><i class="far fa-window-close"></i></div>
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-lg-8">
                                        <div class="modal-body">
                                            <!-- Project Details Go Here-->
                                            <h2 class="text-uppercase">'.$projectName.'</h2>
                                            <p class="item-intro text-muted">'.$modalSubTitle.'</p>
                                            <div id="carouselSlidesOnly" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                    '.$this->listToStringCarousel($modalCarouselItemList).'
                                                </div>
                                                <p>'.$modalBody.'</p>
                                                <ul class="list-inline">
                                                    '.$this->listToStringLi($liList).'
                                                </ul>
                                                <button class="btn btn-primary" data-dismiss="modal" type="button">
                                                    <i class="fas fa-times mr-1"></i>
                                                    '.$closeModalText.'
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>'
        ;

        return [
            "html" => $this->html,
            "modal" => $this->modals,
        ];
    }

    private function listToStringLi(array $listLi){
        $html = "";

        foreach ($listLi as $li) {
            $html .= "
                <li>".$li."</li>
            ";
        }

        return $html;
    }

    private function listToStringCarousel(array $listCarouselItem){
        $html = "";

        foreach ($listCarouselItem as $carouselItem) {
            $html .= '
                <div class="carousel-item active">
                    <img src="assets/img/'.$carouselItem->url.'" class="d-block w-100" alt="'.$carouselItem->alt.'">
                </div>
            ';
        }

        return $html;
    }


}