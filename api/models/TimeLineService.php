<?php

class TimeLineService {
    public $html;

    function __construct(PictureItem $pictureItem, string $date, string $title, string $body){

        return $this->html = '
        <li class="###class###">
            <div class="timeline-image"><img class="rounded-circle img-fluid" src="assets/img/'.$pictureItem->url.'" alt="'.$pictureItem->alt.'" /></div>
            <div class="timeline-panel">
                <div class="timeline-heading">
                    <h4>'.$date.'</h4>
                    <h4 class="subheading">'.$title.'</h4>
                </div>
                <div class="timeline-body"><p class="text-muted">'.$body.'</p></div>
            </div>
        </li>
    ';;
    }
}