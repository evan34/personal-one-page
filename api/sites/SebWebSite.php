<?php
include "api/models/PictureItem.php";
include "api/models/CardService.php";
include "api/models/CircleService.php";
include "api/models/TimeLineService.php";
include "api/models/TeamElement.php";
include 'api/models/TeamService.php';


class WebSite {
    public $cssFile;
    public $title;
    public $navBarLogo;
    public $headerSubMenu;
    public $headerMenu;
    public $headerButton;

    // navigation menu
    public $servicesMenu;
    public $portfolioMenu;
    public $aboutMenu;
    public $teamMenu;

    // Sections variables : by menu hooks
        // #services
        public $servicesSectionTitle;
        public $servicesSectionSubTitle;
        public $servicesSectionHtml;
        // #portfolio
        public $portfolioSectionTitle;
        public $portfolioSectionSubTitle;
        public $portfolioSectionHtml;
        // #about
        public $aboutSectionTitle;
        public $aboutSectionSubTitle;
        public $aboutSectionHtml;
        public $aboutSectionFinalCircle;

        // #team
        public $teamSectionTitle;
        public $teamSectionSubTitle;
        public $teamSectionHtml;
        public $teamSectionFinalText;

        // #contact

    
    function __construct(){
        $this->title = "Busschot.fr";
        $this->cssFile = "seb.css";
        $this->navBarLogo = "navbar-logo.svg";
        $this->headerSubMenu = "Soyez les bienvenus !";
        $this->headerMenu = "3, 2, 1, Programmez !";
        $this->headerButton = "À mon propos";
        
        // menu
        
        $this->servicesMenu = "MON PROFIL";
        $this->portfolioMenu = "Portfolio";
        $this->aboutMenu = "A mon propos";
        $this->teamMenu = "Collaborateurs";
        
        // #services
        $this->servicesSectionTitle = "final seb = new dev(doc)";
        $this->servicesSectionSubTitle = "De vendeur à développeur... de travail à passion...";
        $this->servicesSectionHtml = $this->serviceSectionMaker();
        
        // #portfolio
        $this->portfolioSectionTitle = "PORTFOLIO";
        $this->portfolioSectionSubTitle = "Dans ma quête de reconversion j'ai mené à bien un certain nombre de projets dont voici les principaux.";
        $this->portfolioSectionHtml = $this->portfolioSectionMaker();

        // #about
        $this->aboutSectionTitle = "Retour vers le futur";
        $this->aboutSectionSubTitle = "Je vais vous exposer rapidement mon cursus professionnel.";
        $this->aboutSectionFinalCircle = 'Je vis<br>de ma passion<br>aujourd\'hui<br><i class="fas fa-cloud-sun"></i>';
        $this->aboutSectionHtml = $this->aboutSectionMaker();

        // #team
        $this->teamSectionTitle = "La dev team";
        $this->teamSectionSubTitle = "Uni par un seul crédo : 'Hello world !'";
        $this->teamSectionHtml = $this->teamSectionMaker();
        $this->teamSectionFinalText = "
        La dream team, unis autours de la même passion : coder. Chaqu'un apportant sa pierre à l'édifice, venant élever le niveau général de par son point de vue spécifique.
        <br>
        <br>Lucie, l'artiste du groupe, qui mange du CSS au petit déjeuner et dinne de l'API.
        <br>
        <br>Evan, le seigneur Sith, armé de ses obscures tableaux. Il mene la vie dure aux problèmes qu'il affronte sans...heu...problème.
        <br>
        <br>Volontaires et percévérants, j'ai grand plaisir à travailler en équipe avec eux !
        ";


    }

    // array[CircleService]
    private function serviceSectionMaker(){
        $html = "";
        
        
        $circles = [
            new CircleService(
                "fa-cash-register",
                "Précédement",
                "Plongé depuis toujours dans le bain du commerce, j'ai passé un BTS en management puis je suis entré dans la vie active. Manutentionaire, hôte d'accueil en SAV, vendeur, responsable puis manageur. J'ai prouvé mon éfficacité et mon investissement sur des postes très variés."
            ),
            new CircleService(
                "fas fa-recycle",
                "Reconversion",
                "En 2018 j'étais à la tête d'un commerce depuis 3 ans et je sentais le besoin de faire autre chose.
                <br><br>J'ai pas mal tatonné, cherché et finalement trouvé !
                <br><br>En autodidacte puis dans une école \"piscine\" j'ai découvert le code et la passion qui va avec !
                "
            ),
            new CircleService(
                "fa-project-diagram",
                "Projets",
                "Actuellement je vise un master en conception logicielle. Je souhaite devenir architecte logiciel et coach agile au sein d'une équipe qui partage la même passion pour l'IT et les nouvelles technologies.
                <br><br>En ce moment je suis en Bachelor 3 DevOps chez EPSI Montpellier."
            ),
        ];

        foreach ($circles as $circle) {
            $html .= $circle->html;
        }

        return $html;

    }

    // array[CardService]
    private function portfolioSectionMaker(){    

        $html = "";
        $modals = "";

        $cards = [
            new CardService(
                "adminFidShop",
                "fa-hand-spock",
                new PictureItem("admin_fidshop.jpg","Fid'shop project picture"),
                "Fid'shop",
                "Projet d'initiation à DART",
                "Fid'shop",
                "(WORK IN PROGRESS) Le système de fidélisation à destination des TPE",
                "Après avoir mangé un bon burger j'ai vu le commerçant écrire mon passage sur un bristol de fidélisation. Le projet était né, développer un écosystème gestion de carte de fidélisation.
                <br><br>Il ne sagit que d'un prétexte pour découvrir DART, le nouveau langage de GOOGLE.
                <br><br>Imaginé sous la forme de 2 applications mobiles Flutter et d'une application web Flutter-beta reposant sur une API Aqueduct. Une approche full-stack et peutêtre notre première publication d'application mobile ?
                <br><br>Projet en cours...",
                [
                    new PictureItem("admin_fidshop.jpg","Fid'shop project picture"),

                ],
                [
                    "Participants : Lucie, Evan et moi",
                    "Langage : DART"
                ],
                "Fermer"
            ),
            new CardService(
                "jlassureAdmin",
                "fa-hand-spock",
                new PictureItem("jlassure.png","jlassure project picture"),
                "CMS JLAssure",
                "Projet de stage sous CodeIgniter",
                "Espace d'administration JLAssure",
                "Un CMS maison sous PHP/JS",
                "À la demande de l'entreprise nous avons conçu et développé une solution permettant de génèrer plusieurs sites sur un même serveur, administrés depuis une interface et gèrant l'envoi de mails automatiques.
                <br><br>La génération des pages web se fait en front grâce à un JSON provennant d'une API.
                <br><br>Ce projet a été présenté lors de ma <a href=\"https://docs.google.com/document/d/1p-b5qTCJdnGVDR_r_o0GoZBNWbSiny-RuEy3CazcqVY/edit?usp=sharing\" target='blanck'>soutenance</a> pour le titre pro de développeur web et web mobile.",
                [
                    new PictureItem("jlassure.png","jlassure project picture"),

                ],
                [
                    "Participants : <a href=\"https://www.linkedin.com/in/paulinericard/\" target='blanck'>Pauline</a> et moi",
                    "Langage : PHP/JS",
                    "FrameWorks / Librairies : CodeIgniter, Gridstack, JQuery"
                ],
                "Fermer"
            ),
            new CardService(
                "jlassureApi",
                "fa-hand-spock",
                new PictureItem("jlassure_api.png","jlassure api project picture"),
                "API JLAssure",
                "Projet de stage sous CodeIgniter",
                "API JLAssure",
                "Une API REST sous CodeIgniter",
                "Nous avons conçu et développé cette API pour qu'elle serve de support au projet de CMS de JLAssure. L'API, adaptée pour être REST, est en capacité de recevoir une page déstructurée et de la rendre restituée tout en évitant les doublons grâce à un pérénnisation incrémentielle des données liées aux pages.
                <br><br>Elle est aussi en mesure d'émetre des mails, précédement enregistrés en HTML, à la suite d'un évenement.
                <br><br>Ce projet a été présenté lors de ma <a href=\"https://docs.google.com/document/d/1p-b5qTCJdnGVDR_r_o0GoZBNWbSiny-RuEy3CazcqVY/edit?usp=sharing\" target='blanck'>soutenance</a> pour le titre pro de développeur web et web mobile.
                ",
                [
                    new PictureItem("jlassure_api.png","jlassure api project picture"),

                ],
                [
                    "Participants : <a href=\"https://www.linkedin.com/in/yoan-cougnaud-61278a189//\" target='blanck'>Yoan</a> et moi",
                    "Langage : PHP",
                    "FrameWorks : CodeIgniter"
                ],
                "Fermer"
            ),
        ];

        foreach ($cards as $card) {
            $html .= $card->html;
            $modals .= $card->modals;
        }

        return [
            "html"=>$html,
            "modals"=>$modals
        ];

    }

    // array[AboutService]
    private function aboutSectionMaker(){
        $html = "";
        $timelines = [
            new TimeLineService(
                new PictureItem("cravatte.jpg","Commerce"),
                "2000-2018",
                "Commerce",
                "Vendeur / Magasinier / Responsable : J'ai changé régulièrement d'entreprises et aquis une bonne expérience dans ce domaine."
            ),
            new TimeLineService(
                new PictureItem("idee.jpg","Idee"),
                "Fin 2018",
                "Reconversion",
                "OpenClassrooms, w3schools, Sololearn... J'ai commencé ma reconversion."
            ),
            new TimeLineService(
                new PictureItem("beweb.png","BeWeb"),
                "2019",
                "BeWeb : l'école de l'apprentissage !",
                "BeWeb est une école \"42\", c'est à dire que l'apprentissage se fait sans cours, par la pratique et l'entre-aide. Les projets variés mont permis de m'initier aux technologies les plus en vogue tels que :
                <br>Symfony, CodeIgniter, NodeJs,<br>JQuery, Angular, React
                <br>Pendant la formation j'ai pu également développer un framework PHP et de le déployer sur un serveur en ligne.
                <br>Une belle expérience !"
            )
        ];

        foreach($timelines as $index => $timeline){
            if($index % 2 != 0){
                $timeline->html = str_replace("###class###","timeline-inverted",$timeline->html);
            }
            $html .= $timeline->html;
        }

        $html .= '
        <li>
            <div class="timeline-image">
                <h4>
                    '.$this->aboutSectionFinalCircle.'
                </h4>
            </div>
        </li>';

        return $html;
    }

    private function teamSectionMaker(){

        $html= "";

        $team = [
            new TeamElement(
                new PictureItem("evan.jpeg","Evan Martinez Photo"),
                "Evan Martinez",
                "Developpeur full-stack",
                "",
                "",
                "https://www.linkedin.com/in/martinezevan/"
            ),
            new TeamElement(
                new PictureItem("lucie.jpeg","Lucie Barthes Photo"),
                "Lucie Barthes",
                "Developpeur full-stack",
                "",
                "",
                "https://www.linkedin.com/in/lucie-barthes/"
            ),
            new TeamElement(
                // exemple avec moi mais ne le supprimez pas ;)
                new PictureItem("seb.jpeg","Sébastien Busschot"),
                "Sébastien Busschot",
                "Développeur full-stack, DevOps",
                "", // N°1 : Twiter n'affichera rien : je ne veux pas afficher twiter
                "http://busschot.fr/", // N°2 : site perso (metttez votre nom de domaine => redirection par cadre sur 5.196.95.21)
                "https://www.linkedin.com/in/sebastien-busschot/l"// N°3 : dois-je préciser? :D
            )
        ];

        foreach ($team as $element) {
            $staf = new TeamService($element);
            $html .= $staf->html;
        }

        return $html;
    }
}

